-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 23, 2021 at 04:11 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_skripsi`
--

-- --------------------------------------------------------

--
-- Table structure for table `alpha`
--

CREATE TABLE `alpha` (
  `id` int(11) NOT NULL,
  `alpha` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `alpha`
--

INSERT INTO `alpha` (`id`, `alpha`) VALUES
(1, 0.1),
(2, 0.2),
(3, 0.3),
(4, 0.4),
(5, 0.5),
(6, 0.6),
(7, 0.7),
(8, 0.8),
(9, 0.9);

-- --------------------------------------------------------

--
-- Table structure for table `data_aktual`
--

CREATE TABLE `data_aktual` (
  `id` int(11) NOT NULL,
  `id_kota` int(11) NOT NULL,
  `id_tahun` int(11) NOT NULL,
  `data_aktual` int(11) NOT NULL,
  `aktif` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `data_aktual`
--

INSERT INTO `data_aktual` (`id`, `id_kota`, `id_tahun`, `data_aktual`, `aktif`) VALUES
(1, 1, 1, 25, 1),
(2, 1, 2, 29, 1),
(3, 1, 3, 26, 1),
(4, 1, 4, 25, 1),
(5, 1, 5, 28, 1),
(6, 1, 6, 31, 1),
(7, 1, 7, 30, 1),
(8, 1, 8, 33, 1),
(9, 1, 9, 49, 1),
(10, 1, 10, 43, 1),
(11, 1, 11, 43, 1),
(12, 1, 12, 74, 1),
(13, 1, 13, 55, 1),
(14, 1, 14, 43, 1),
(15, 1, 15, 46, 1),
(16, 2, 1, 7, 1),
(17, 2, 2, 9, 1),
(18, 2, 3, 9, 1),
(19, 2, 4, 10, 1),
(20, 2, 5, 9, 1),
(21, 2, 6, 11, 1),
(22, 2, 7, 10, 1),
(23, 2, 8, 8, 1),
(24, 2, 9, 11, 1),
(25, 2, 10, 8, 1),
(26, 2, 11, 7, 1),
(27, 2, 12, 9, 1),
(28, 2, 13, 9, 1),
(29, 2, 14, 10, 1),
(30, 2, 15, 10, 1),
(31, 3, 1, 5, 1),
(32, 3, 2, 5, 1),
(33, 3, 3, 8, 1),
(34, 3, 4, 7, 1),
(35, 3, 5, 9, 1),
(36, 3, 6, 8, 1),
(37, 3, 7, 11, 1),
(38, 3, 8, 12, 1),
(39, 3, 9, 12, 1),
(40, 3, 10, 11, 1),
(41, 3, 11, 9, 1),
(42, 3, 12, 10, 1),
(43, 3, 13, 10, 1),
(44, 3, 14, 10, 1),
(45, 3, 15, 11, 1);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(4, 'members', 'General User');

-- --------------------------------------------------------

--
-- Table structure for table `groups_permissions`
--

CREATE TABLE `groups_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `perm_id` int(11) NOT NULL,
  `value` tinyint(4) DEFAULT 0,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groups_permissions`
--

INSERT INTO `groups_permissions` (`id`, `group_id`, `perm_id`, `value`, `created_at`, `updated_at`) VALUES
(56, 1, 23, 1, 1606837298, 1606837298),
(57, 1, 24, 1, 1606837298, 1606837298),
(58, 1, 9, 1, 1606837298, 1606837298),
(59, 1, 12, 1, 1606837298, 1606837298),
(60, 1, 11, 1, 1606837298, 1606837298),
(61, 1, 10, 1, 1606837298, 1606837298),
(62, 1, 14, 1, 1606837298, 1606837298),
(63, 1, 16, 1, 1606837298, 1606837298),
(64, 1, 13, 1, 1606837298, 1606837298),
(65, 1, 15, 1, 1606837298, 1606837298),
(66, 1, 21, 1, 1606837298, 1606837298),
(67, 1, 22, 1, 1606837298, 1606837298),
(68, 1, 18, 1, 1606837298, 1606837298),
(69, 1, 20, 1, 1606837298, 1606837298),
(70, 1, 17, 1, 1606837298, 1606837298),
(71, 1, 19, 1, 1606837298, 1606837298),
(73, 4, 23, 1, 1606861619, 1606861619),
(74, 4, 24, 1, 1606861619, 1606861619);

-- --------------------------------------------------------

--
-- Table structure for table `hasil_peramalan`
--

CREATE TABLE `hasil_peramalan` (
  `id` int(11) NOT NULL,
  `id_data_aktual` int(11) NOT NULL,
  `id_alpha` int(11) NOT NULL,
  `pem1` float NOT NULL,
  `pem2` float NOT NULL,
  `expo1` float NOT NULL,
  `expo2` float NOT NULL,
  `hasil` float NOT NULL,
  `fe` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `hasil_peramalan`
--

INSERT INTO `hasil_peramalan` (`id`, `id_data_aktual`, `id_alpha`, `pem1`, `pem2`, `expo1`, `expo2`, `hasil`, `fe`) VALUES
(16, 1, 1, 25, 25, 25, 0, 25, 0),
(17, 2, 1, 25.4, 25.04, 25.76, 0.04, 25.8, 11.0345),
(18, 3, 1, 25.46, 25.082, 25.838, 0.042, 25.88, 0.461538),
(19, 4, 1, 25.414, 25.1152, 25.7128, 0.0332, 25.746, 2.984),
(20, 5, 1, 25.6726, 25.1709, 26.1743, 0.05574, 26.23, 6.32143),
(21, 6, 1, 26.2053, 25.2744, 27.1363, 0.10344, 27.2397, 12.1299),
(22, 7, 1, 26.5848, 25.4054, 27.7642, 0.131043, 27.8952, 7.01589),
(23, 8, 1, 27.2263, 25.5875, 28.8651, 0.18209, 29.0472, 11.9781),
(24, 9, 1, 29.4037, 25.9691, 32.8383, 0.381618, 33.2199, 32.2043),
(25, 10, 1, 30.7633, 26.4485, 35.0781, 0.479419, 35.5575, 17.3081),
(26, 11, 1, 31.987, 27.0024, 36.9716, 0.553844, 37.5254, 12.7316),
(27, 12, 1, 36.1883, 27.921, 44.4556, 0.91859, 45.3742, 38.6835),
(28, 13, 1, 38.0695, 28.9358, 47.2031, 1.01485, 48.2179, 12.331),
(29, 14, 1, 38.5625, 29.8985, 47.2265, 0.962668, 48.1892, 12.0679),
(30, 15, 1, 39.3063, 30.8393, 47.7733, 0.940776, 48.714, 5.90006),
(131, 1, 5, 25, 25, 25, 0, 25, 0),
(132, 2, 5, 27, 26, 28, 1, 29, 0),
(133, 3, 5, 26.5, 26.25, 26.75, 0.25, 27, 3.84615),
(134, 4, 5, 25.75, 26, 25.5, -0.25, 25.25, 1),
(135, 5, 5, 26.875, 26.4375, 27.3125, 0.4375, 27.75, 0.892857),
(136, 6, 5, 28.9375, 27.6875, 30.1875, 1.25, 31.4375, 1.41129),
(137, 7, 5, 29.4688, 28.5781, 30.3594, 0.890625, 31.25, 4.16667),
(138, 8, 5, 31.2344, 29.9062, 32.5625, 1.32812, 33.8906, 2.69886),
(139, 9, 5, 40.1172, 35.0117, 45.2227, 5.10547, 50.3281, 2.71046),
(140, 10, 5, 41.5586, 38.2852, 44.832, 3.27344, 48.1055, 11.8732),
(141, 11, 5, 42.2793, 40.2822, 44.2764, 1.99707, 46.2734, 7.61265),
(142, 12, 5, 58.1396, 49.2109, 67.0684, 8.92871, 75.9971, 2.69874),
(143, 13, 5, 56.5698, 52.8904, 60.2493, 3.67944, 63.9287, 16.234),
(144, 14, 5, 49.7849, 51.3376, 48.2322, -1.55273, 46.6794, 8.55684),
(145, 15, 5, 47.8925, 49.6151, 46.1699, -1.7226, 44.4473, 3.37551);

-- --------------------------------------------------------

--
-- Table structure for table `kota`
--

CREATE TABLE `kota` (
  `id` int(11) NOT NULL,
  `kota` varchar(255) NOT NULL,
  `keterangan` text NOT NULL,
  `aktif` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kota`
--

INSERT INTO `kota` (`id`, `kota`, `keterangan`, `aktif`) VALUES
(1, 'Surabaya', 'Kota Surabaya', 1),
(2, 'Sidoarjo', 'Kabupaten Sidoarjo', 1),
(3, 'Gresik', 'Kabupaten Gresik', 1);

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(11) NOT NULL,
  `perm_key` varchar(30) NOT NULL,
  `perm_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `perm_key`, `perm_name`) VALUES
(4, 'pengaturan-hak_akses-create', 'Create Hak Akses'),
(5, 'pengaturan-hak_akses-delete', 'Delete Hak Akses'),
(6, 'pengaturan-pengguna-create', 'Create Pengguna'),
(7, 'master-anggota-create', 'Create Anggota'),
(8, 'pengaturan-hak_akses-update', 'Update Hak Akses'),
(9, 'master-kota-create', 'Create Master Kota'),
(10, 'master-kota-update', 'Update Master Kota'),
(11, 'master-kota-read', 'Read Master Kota'),
(12, 'master-kota-delete', 'Delete Master Kota'),
(13, 'master-tahun-read', 'Read Master Tahun'),
(14, 'master-tahun-create', 'Create Master Tahun'),
(15, 'master-tahun-update', 'Update Master Tahun'),
(16, 'master-tahun-delete', 'Delete Master Tahun'),
(17, 'transaksi-aktual-read', 'Read Transaksi Aktual'),
(18, 'transaksi-aktual-create', 'Create Transaksi Aktual'),
(19, 'transaksi-aktual-update', 'Update Transaksi Aktual'),
(20, 'transaksi-aktual-delete', 'Delete Transaksi Aktual'),
(21, 'pengaturan-hak_akses-read', 'Read Pengaturan Hak Akses Read'),
(22, 'pengaturan-pengguna-read', 'Read Pengaturan Pengguna'),
(23, 'dashboard-beranda-read', 'Read Beranda'),
(24, 'laporan-peramalan-read', 'Read Laporan Peramalan');

-- --------------------------------------------------------

--
-- Table structure for table `tahun`
--

CREATE TABLE `tahun` (
  `id` int(11) NOT NULL,
  `tahun` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `aktif` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tahun`
--

INSERT INTO `tahun` (`id`, `tahun`, `keterangan`, `aktif`) VALUES
(1, 2005, '', 1),
(2, 2006, '', 1),
(3, 2007, '', 1),
(4, 2008, '', 1),
(5, 2009, '', 1),
(6, 2010, '', 1),
(7, 2011, '', 1),
(8, 2012, '', 1),
(9, 2013, '', 1),
(10, 2014, '', 1),
(11, 2015, '', 1),
(12, 2016, '', 1),
(13, 2017, '', 1),
(14, 2018, '', 1),
(15, 2019, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(254) NOT NULL,
  `activation_selector` varchar(255) DEFAULT NULL,
  `activation_code` varchar(255) DEFAULT NULL,
  `forgotten_password_selector` varchar(255) DEFAULT NULL,
  `forgotten_password_code` varchar(255) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_selector` varchar(255) DEFAULT NULL,
  `remember_code` varchar(255) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `email`, `activation_selector`, `activation_code`, `forgotten_password_selector`, `forgotten_password_code`, `forgotten_password_time`, `remember_selector`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'admin', '$2y$12$poAjCwNFj7hUEeJI4HZrL.eu0gb/S6qGLVIjy4Ry4MtIQPfpAX/H6', 'papsi.its@gmail.com', NULL, '', NULL, NULL, NULL, NULL, NULL, 1268889823, 1614070000, 1, 'Admin', 'Keren', 'ADMIN', '081234504408'),
(2, '::1', 'tono', '$2y$10$Gh4a03rc5Bg6jbFVpdwA/e7W1ueTHgTKuiXe2rCw1Jtezm/KQ/dne', 'tono@contoh.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1562561882, NULL, 1, 'Toni', 'Hartono', '', ''),
(3, '::1', 'john', '$2y$10$noNe0HyS/GIMWfI0NXONzeaHLnWxSDEtHG2sGVMxvBFQyxjCJEShe', 'john@example.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1562590867, NULL, 1, 'John', 'Doe', '', '0'),
(5, '::1', 'arnoldarmando07', '$2y$10$u.GYSQfF1w/scDcs1TNVp.PLKOKowLPuL3JSw1ZmYJlXI2pox0OXe', 'arnoldarmando07@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1614014953, 1614054883, 1, 'Arnold', 'Armando', 'PAPSI', '087854769001');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(57, 1, 1),
(52, 2, 4),
(54, 3, 4),
(61, 5, 4);

-- --------------------------------------------------------

--
-- Table structure for table `users_permissions`
--

CREATE TABLE `users_permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `perm_id` int(11) NOT NULL,
  `value` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `alpha`
--
ALTER TABLE `alpha`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_aktual`
--
ALTER TABLE `data_aktual`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups_permissions`
--
ALTER TABLE `groups_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roleID_2` (`group_id`,`perm_id`);

--
-- Indexes for table `hasil_peramalan`
--
ALTER TABLE `hasil_peramalan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kota`
--
ALTER TABLE `kota`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permKey` (`perm_key`);

--
-- Indexes for table `tahun`
--
ALTER TABLE `tahun`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_username` (`username`),
  ADD UNIQUE KEY `uc_email` (`email`),
  ADD UNIQUE KEY `uc_activation_selector` (`activation_selector`),
  ADD UNIQUE KEY `uc_forgotten_password_selector` (`forgotten_password_selector`),
  ADD UNIQUE KEY `uc_remember_selector` (`remember_selector`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- Indexes for table `users_permissions`
--
ALTER TABLE `users_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `userID` (`user_id`,`perm_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `alpha`
--
ALTER TABLE `alpha`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `data_aktual`
--
ALTER TABLE `data_aktual`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `groups_permissions`
--
ALTER TABLE `groups_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT for table `hasil_peramalan`
--
ALTER TABLE `hasil_peramalan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=146;

--
-- AUTO_INCREMENT for table `kota`
--
ALTER TABLE `kota`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `tahun`
--
ALTER TABLE `tahun`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `users_permissions`
--
ALTER TABLE `users_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
