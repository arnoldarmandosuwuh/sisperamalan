<div class="row">
  <div class="col-md-12">

  <?php echo show_alert($this->session->flashdata()); ?>

    <div class="card">
      <div class="card-header">
        <i class="nav-icon icon-chart"></i> <?php echo show($title); ?>
      </div>

      <!-- form hitung peramalan -->

      <form class="form-horizontal" id="form-peramalan" method="post" action="<?php echo base_url('laporan/peramalan'); ?>">
        <div class="row">
          <div class="card-body">
            <div class="col-md-12">
              
              <div class="form-group row">
                <label for="id_kota" class="control-label col-md-2"> Kota </label>
                <div class="col-md-10">
                  <select class="form-control app-select2" id="id_kota" name="id_kota">
                    <option value="">-- Silahkan pilih kota --</option>
                
                    <?php foreach ($list_kota as $kota) : ?>
                      
                      <option value="<?php echo $kota->id; ?>"><?php show($kota->kota); ?></option>

                    <?php endforeach; ?>
                  
                  </select>
                  <?php echo form_error('id_kota', '<label class="text-danger">', '</label>'); ?>
                </div>  
              </div>

              <div class="form-group row">
                <label for="periode1" class="control-label col-md-2"> Data dari </label>
                <div class="col-md-4">
                  <select class="form-control app-select2" id="periode1" name="periode1">
                    <option value="">-- Silahkan pilih tahun awal --</option>
                    <?php foreach ($list_tahun as $tahun) : ?>
                        
                        <option value="<?php echo $tahun->id; ?>"><?php show($tahun->tahun); ?></option>

                      <?php endforeach; ?>
                  
                  </select>
                  <?php echo form_error('periode1', '<label class="text-danger">', '</label>'); ?>
                </div>
                <label for="periode2" class="control-label col-md-2"> Sampai </label>
                <div class="col-md-4">
                  <select class="form-control app-select2" id="periode2" name="periode2">
                    <option value="">-- Silahkan pilih tahun akhir --</option>
                    <?php foreach ($list_tahun as $tahun) : ?>
                      
                      <option value="<?php echo $tahun->id; ?>"><?php show($tahun->tahun); ?></option>

                    <?php endforeach; ?>
                  
                  </select>
                  <?php echo form_error('periode2', '<label class="text-danger">', '</label>'); ?>
                </div>  
              </div>

              <div class="form-group row">
                <label for="alpha" class="control-label col-md-2"> &alpha; </label>
                <div class="col-md-10">
                  <?php if ($this->ion_auth->is_admin()) : ?>
                    <select class="form-control app-select2" id="alpha" name="alpha">
                      <option value="">-- Silahkan pilih &alpha; --</option>
                      <?php foreach ($list_alpha as $alpha) : ?>
                        
                        <option value="<?php echo $alpha->id; ?>"><?php show($alpha->alpha); ?></option>

                      <?php endforeach; ?>

                    </select>
                  <?php else : ?>
                    <input type="text" class="form-control" id="id_alpha" name="id_alpha" value="0.5" readonly />
                    <input type="hidden" class="form-control" id="alpha" name="alpha" value="5" readonly />
                  <?php endif;?>
                  <?php echo form_error('alpha', '<label class="text-danger">', '</label>'); ?>
                </div>  
              </div>

              <div class="form-group row">
                <div class="col-md-10">
                  <label class="text-danger">Keterangan : Hasil peramalan dengan mape terbaik adalah dengan &alpha; 0.5</label>
                </div>
              </div>
              
              <button class="btn btn-primary pull-right"  type="submit"><i class="fa fa-save"></i> Ramal </button>
              
            </div>
          </div>
        </div>
      </form>
    </div>
      <!-- akhir form hitung peramalan -->
    <?php if (!empty($list_data)) :?>
      <!-- table hasil -->
    <div class="card">
        <div class="card-header">
            <i class="nav-icon icon-chart"></i><?php show($title); ?>
        </div>
        <div class="row">
            <div class="card-body">
                <div class="col-md-12">
                    <h5 class="card-title">Table Hasil Peramalan</h5>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover table-condensed peramalan-datatable">
                            <caption>Hasil peramalan data pendaftaran PAPSI-ITS dari Kota <?php show($list_data[0]['kota']); ?> dengan nilai &alpha; <?php show($list_data[0]['alpha']); ?> memiliki nilai MAPE sebesar <?php show(number_format($mape, 2, ',','.').'%'); ?></caption>
                            <thead>
                                <tr>
                                    <th class="text-center" style="width: 10px;">No</th>
                                    <th class="text-center"> &alpha; </th>
                                    <th class="text-center">Tahun</th>
                                    <th class="text-center">Data Aktual</th>
                                    <th class="text-center">Hasil Peramalan</th>
                                    <th class="text-center">FE</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no=1; foreach ($list_data as $value) : ?>
                                    <tr>
                                        <td class="text-center"><?php show($no++); ?></td>
                                        <td class="text-center"><?php show($value['alpha']); ?></td>
                                        <td class="text-center"><?php show($value['tahun']); ?></td>
                                        <td class="text-center"><?php show($value['data_aktual']); ?></td>
                                        <td class="text-center"><?php show($value['hasil']); ?></td>
                                        <td class="text-center"><?php show($value['fe']); ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                            <tfoot>
                                    <tr>
                                        <td colspan=5 class="text-right">MAPE</td>
                                        <td class="text-center"><?php show(number_format($mape, 2, ',','.')); ?></td>
                                    </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        
      <div class="row">
        <div class="card-body">
          <div class="col-md-12">
              <h5 class="card-title">Kurva Fitting</h5>
              <canvas id="peramalan-chart"></canvas>
          </div>
        </div>
      </div>
    </div>
      <!-- akhir table hasil -->
    <?php endif; ?>
  </div>
</div>
<script>
  window.onload = function() {
    var ctx = document.getElementById('peramalan-chart').getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',
    
        // The data for our dataset
        data: {
            labels: <?php echo json_encode($periode); ?>,
            datasets: [
              {
                label: 'Data Aktual',
                backgroundColor: 'rgb(255, 255, 255, 0.1)',
                borderColor: 'rgb(32, 216, 140)',
                data: <?php echo json_encode($aktual); ?>,
              },
              {
                label: 'Hasil Peramalan',
                backgroundColor: 'rgb(255, 255, 255, 0.1)',
                borderColor: 'rgb(32, 168, 216)',
                data: <?php echo json_encode($ramal); ?>,
              }
            ]
        },
    
        // Configuration options go here
        options: {
          responsive: true,
          scales: {
            yAxes: [{
              scaleLabel: {
                display: true,
                labelString: 'Data Pendaftaran',
              }
            }],
            xAxes: [{
              scaleLabel: {
                display: true,
                labelString: 'Tahun',
              }
            }]
          } 
        }
    });
  }
</script>