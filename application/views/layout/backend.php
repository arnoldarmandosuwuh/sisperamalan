<!DOCTYPE html>
<html lang="id">
  <head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title><?php echo show($title) .' | ' . $this->config->item('app_title'); ?></title>
    <?php foreach ($list_css as $key_css => $url_css) : ?>
    <link href="<?php echo $url_css; ?>" rel="stylesheet">
    <?php endforeach; ?>
  </head>
  <body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
    <header class="app-header navbar">
      <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
        <span class="navbar-toggler-icon"></span>
      </button>
      <a class="navbar-brand" href="#">
        <img class="navbar-brand-full" src="<?php echo base_url('assets/img/brand/logo.svg'); ?>" width="89" height="25" alt="CoreUI Logo">
        <img class="navbar-brand-minimized" src="<?php echo base_url('assets/img/brand/sygnet.svg'); ?>" width="30" height="30" alt="CoreUI Logo">
      </a>
      <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
        <span class="navbar-toggler-icon"></span>
      </button>
      <ul class="nav navbar-nav ml-auto">
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            <strong class="view-full" style="display: inline-block;"><?php show($this->ion_auth->user()->row()->first_name); ?></strong>  
            <img class="img-avatar" src="<?php echo base_url('assets/img/avatars/user.jpg'); ?>" alt="admin@bootstrapmaster.com">
          </a>
          <div class="dropdown-menu dropdown-menu-right">
            <div class="dropdown-header text-center">
              <strong>User Login</strong>
              <strong class="view-small" style="display: none">
              <?php show($this->ion_auth->user()->row()->first_name); ?></strong>
           </div>
            <a class="dropdown-item" href="<?php echo base_url('auth/change-password'); ?>">
              <i class="fa fa-lock"></i> Ganti Password
            </a>
            <a class="dropdown-item" href="<?php echo base_url('auth/logout'); ?>">
              <i class="fa fa-sign-out"></i> Logout
            </a>
          </div>
        </li>
      </ul>
    </header>
    <div class="app-body">
      <div class="sidebar">
        <nav class="sidebar-nav">
          <ul class="nav">
            
            <li class="nav-title">Menu Utama</li>

            <?php foreach ($dashboard_menu as $title => $menu) : ?>

                <?php 
                  $cek_permission = FALSE;
                  foreach ($menu['permission'] as $key) {
                      $cek_permission = $cek_permission || $this->ion_auth_acl->has_permission($key);
                  }
                  if ($cek_permission) : 
                ?>
            
                <li class="nav-item nav-dropdown <?php echo !empty($menu['submenu']) ? expand_menu($menu['submenu']) : ''; ?>">
                  
                  <a class="nav-link <?php echo !empty($menu['submenu']) ? 'nav-dropdown-toggle': '' ; ?>" href="<?php echo !empty($menu['url']) ? $menu['url'] : '#' ; ?>">
                    <i class="<?php echo $menu['icon']; ?>"></i> <?php echo $title; ?>
                  </a>

                  <?php if (!empty($menu['submenu'])) : ?>

                      <ul class="nav-dropdown-items">
                        
                        <?php foreach ($menu['submenu'] as $submenu_title => $submenu) : ?>
                            <li class="nav-item">
                              <a class="nav-link <?php echo !empty($submenu['url']) ? active_menu($submenu['url']) : '' ; ?>" href="<?php echo $submenu['url']; ?>">
                                <i class="<?php echo $submenu['icon']; ?>"></i> <?php echo $submenu_title; ?>
                              </a>
                            </li>
                        <?php endforeach; ?>

                      </ul>

                <?php endif; ?>

                </li>
              <?php endif; ?>
            <?php endforeach; ?>

          </ul>
        </nav>
        <button class="sidebar-minimizer brand-minimizer" type="button"></button>
      </div>
      <main class="main">
        <!-- Breadcrumb-->
        <ol class="breadcrumb">
        <?php
          if (isset($breadcrumbs)) :
            foreach ($breadcrumbs as $key_bread => $breadcrumb) :
        ?>

          <?php
            end($breadcrumbs);
            if ($key_bread == key($breadcrumbs)) :
          ?>
            <li class="breadcrumb-item">
              <?php echo $breadcrumb['title'] ?>
            </li>
        <?php else: ?>
            <li class="breadcrumb-item">
              <a href="<?php echo $breadcrumb['url']; ?>"><?php echo $breadcrumb['title'] ?></a>
            </li>
          <?php
              endif;
            endforeach;
          endif;
        ?>
          &nbsp;
          <!-- Breadcrumb Menu-->
        </ol>
        <div class="container-fluid">
          <div class="animated fadeIn">
            <?php echo $content; ?>
          </div>
        </div>
      </main>
      <aside class="aside-menu">
        <!-- Tab panes-->
      </aside>
    </div>
    <footer class="app-footer">
      <div>
        Sisperamalan
        <span>&copy; 2020 <a href="https://linktr.ee/arnoldarmandosuwuh" target="_blank">Arnold Armando Suwuh</a></span>
      </div>
      <div class="ml-auto">
        <span>Powered by</span>
        <a href="https://coreui.io" target="_blank">CoreUI</a>
      </div>
    </footer>
    
    <script type="text/javascript">
      var base_url = '<?php echo base_url(); ?>';
    </script>

    <!-- generate js yang akan di load di halaman backend -->
    <?php foreach ($list_js as $key_js => $url_js) : ?>
    <script src="<?php echo $url_js; ?>"></script>
    <?php endforeach; ?>

  </body>
</html>
