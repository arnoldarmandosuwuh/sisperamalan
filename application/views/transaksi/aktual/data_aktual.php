<div class="row">
  <div class="col-md-12">

    <?php echo show_alert($this->session->flashdata()); ?>

    <div class="card">
      <div class="card-header">
        <i class="nav-icon icon-badge"></i> <?php echo show($title); ?>
        <div class="pull-right">
          <?php if ($this->ion_auth_acl->has_permission('transaksi-aktual-create')) : ?>
            <a href="<?php echo base_url('transaksi/aktual/import'); ?>" class="btn btn-success btn-sm"><i class="fa fa-upload"></i> Import</a>
            <a href="<?php echo base_url('transaksi/aktual/add'); ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Tambah</a>
          <?php endif; ?>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-bordered table-striped table-condensed table-datatable">
                <thead>
                  <tr>
                    <th style="width: 10px;">No</th>
                    <th>Kota</th>
                    <th>Tahun</th>
                    <th>Data Pendaftar</th>
                    <?php if ($this->ion_auth_acl->has_permission('transaksi-aktual-update') || $this->ion_auth_acl->has_permission('transaksi-aktual-delete')) : ?>
                      <th style="width: 10px;">Pilihan</th>
                    <?php endif; ?>
                  </tr>
                </thead>
                <tbody>
                  <?php $no = 1;
                  foreach ($list_data as $data) : ?>
                    <tr>
                      <td class="text-center"><?php show($no++); ?></td>
                      <td><?php show($data->kota); ?></td>
                      <td><?php show($data->tahun); ?></td>
                      <td><?php show($data->data_aktual); ?></td>
                      <?php if ($this->ion_auth_acl->has_permission('transaksi-aktual-update') || $this->ion_auth_acl->has_permission('transaksi-aktual-delete')) : ?>
                        <td class="text-center">
                          <div class="dropdown dropleft">
                            <button class="btn btn-secondary btn-sm" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="icon-menu"></i>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                              <?php if ($this->ion_auth_acl->has_permission('transaksi-aktual-update')) : ?>
                                <a class="dropdown-item" href="<?php echo base_url('transaksi/aktual/edit/' . $data->id); ?>"><i class="fa fa-edit"></i> Edit</a>
                              <?php endif; ?>
                              <?php if ($this->ion_auth_acl->has_permission('transaksi-aktual-delete')) : ?>
                                <a class="dropdown-item btn-delete" href="<?php echo base_url('transaksi/aktual/delete/' . $data->id); ?>"><i class="fa fa-trash"></i> Hapus</a>
                              <?php endif; ?>
                            </div>
                          </div>
                        </td>
                      <?php endif; ?>
                    </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>