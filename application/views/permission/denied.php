<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Akses Ditolak!</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.css'); ?>">
    <style type="text/css">
        #main {
            height: 100vh;
        }
    </style>
</head>
<body>
<!------ Include the above in your HEAD tag ---------->

<div class="d-flex justify-content-center align-items-center" id="main">
    <h1 class="mr-3 pr-3 align-top border-right inline-block align-content-center">401</h1>
    <div class="inline-block align-middle">
        <h2 class="font-weight-normal lead" id="desc">Access denied!</h2>
    </div>
</div>
</body>
</html>