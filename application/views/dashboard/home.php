<div class="row">
  <div class="col-md-12">

    <?php echo show_alert($this->session->flashdata()); ?>

  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="jumbotron bg-light">
      <h1 class="display-5 text-primary">Selamat Datang, <?php show($name_user); ?></h1>
      <p class="lead text-primary">Sistem Informasi Peramalan Jumlah Pendaftaran Mahasiswa PAPSI-ITS menggunakan metode Double Exponential Smoothing Satu Parameter</p>
      <hr class="my-4" />
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-4">
    <div class="card card-accent-primary">
      <div class="card-header">
        <i class="nav-icon icon-chart"></i><?php show($title_card); ?>
      </div>
      <div class="row">
        <div class="card-body">
          <canvas id="data-chart"></canvas>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  window.onload = function() {
    var ctx = document.getElementById('data-chart').getContext('2d');
    var colors = [];
    var dataKota = <?php echo json_encode($kota) ?>;

    for (let i = 0; i < dataKota.length; i++) {
      colors.push('#' + Math.floor(Math.random() * 16777215).toString(16));
    }
    var chart = new Chart(ctx, {
      // The type of chart we want to create
      type: 'pie',

      // The data for our dataset
      data: {
        labels: dataKota,
        datasets: [{
          label: dataKota,
          backgroundColor: colors,
          hoverBackgroundColor: colors,
          data: <?php echo json_encode($data_aktual); ?>,
        }, ]
      },

      // Configuration options go here
      options: {
        responsive: true,
        animateRotate: true
      }
    });
  }
</script>