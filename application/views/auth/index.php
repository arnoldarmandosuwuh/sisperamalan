<div class="row">
	<div class="col-md-12">

    <?php if (!empty($message_success)) : ?>
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <?php echo $message_success; ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    <?php endif; ?>

		<div class="card">
      <div class="card-header">
        <i class="nav-icon icon-user"></i> Data Pengguna

        <div class="pull-right">
          <a class="btn btn-sm btn-primary" href="<?php echo base_url('auth/create_user'); ?>">
            <i class="icon-plus"></i> Tambah Pengguna
          </a>

          <a class="btn btn-sm btn-success" href="<?php echo base_url('auth/create_group'); ?>">
            <i class="icon-plus"></i> Tambah Grup
          </a>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-12">
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <nav>
              <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active" id="nav-pengguna-tab" data-toggle="tab" href="#nav-pengguna" role="tab" aria-controls="nav-home" aria-selected="true">Pengguna</a>
                <a class="nav-item nav-link" id="nav-grup-tab" data-toggle="tab" href="#nav-grup" role="tab" aria-controls="nav-profile" aria-selected="false">Grup</a>
              </div>
            </nav>
            <div class="tab-content" id="nav-tabContent">
              <div class="tab-pane fade show active" id="nav-pengguna" role="tabpanel" aria-labelledby="nav-pengguna-tab">
                <div class="table-responsive">
                <table class="table table-bordered table-striped table-condensed table-datatable">
                  <thead>
                    <tr>
                      <th style="width: 20px;">No</th>
                      <th><?php echo lang('index_fname_th');?></th>
                      <th><?php echo lang('index_lname_th');?></th>
                      <th><?php echo lang('index_email_th');?></th>
                      <th><?php echo lang('index_groups_th');?></th>
                      <th><?php echo lang('index_status_th');?></th>
                      <th style="width: 20px;"><?php echo lang('index_action_th');?></th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php
                  $no = 1;
                  foreach ($users as $user):?>
                    <tr>
                      <td class="text-center"><?php echo $no++; ?></td>
                            <td><?php echo htmlspecialchars($user->first_name,ENT_QUOTES,'UTF-8');?></td>
                            <td><?php echo htmlspecialchars($user->last_name,ENT_QUOTES,'UTF-8');?></td>
                            <td><?php echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');?></td>
                      <td>
                        <?php foreach ($user->groups as $group):?>
                          <?php echo anchor("auth/edit_group/".$group->id, htmlspecialchars($group->name, ENT_QUOTES,'UTF-8'), array('class'=>'badge badge-primary')) ;?>
                                <?php endforeach?>
                      </td>
                      <td><?php echo ($user->active) ? anchor("auth/deactivate/".$user->id, '<i class="icon-check"></i> Aktif', 'class="badge badge-success"') : anchor("auth/activate/". $user->id, '<i class="icon-close"></i> Nonaktif', 'class="badge badge-danger"');?></td>
                      <td class="text-center">
                        <div class="dropdown dropleft">
                          <button class="btn btn-secondary btn-sm" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="icon-menu"></i>
                          </button>
                          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                          <?php echo anchor("auth/user-permissions/".$user->id, '<i class="fa fa-key"></i> Hak Akses', array('class' => 'dropdown-item')) ;?>
                          <?php echo anchor("auth/edit_user/".$user->id, '<i class="fa fa-edit"></i> Edit', array('class' => 'dropdown-item')) ;?>
                          <?php if ($user->username !== 'admin'): ?>
                              <?php echo anchor("auth/delete_user/".$user->id, '<i class="fa fa-trash"></i> Hapus', array('class' => 'dropdown-item btn-delete')) ;?>
                          <?php endif; ?>
                          </div>
                        </div>
                      </td>
                    </tr>
                  <?php endforeach;?>
                  </tbody>
                </table>
                </div>
              </div>

              <div class="tab-pane fade" id="nav-grup" role="tabpanel" aria-labelledby="nav-grup-tab">
                <div class="table-responsive">
                  <table class="table table-bordered table-striped table-condensed table-datatable">
                    <thead>
                      <tr>
                        <th style="width:20px;">No</th>
                        <th>Grup</th>
                        <th>Deskripsi</th>
                        <th style="width: 30px;">Pilihan</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php
                        $no_group = 1; 
                        foreach ($groups as $key_group => $group) :
                    ?>
                      <tr>
                        <td class="text-center"><?php echo $no_group++; ?></td>
                        <td><?php echo $group->name; ?></td>
                        <td><?php echo $group->description; ?></td>
                        <td class="text-center">
                          <div class="dropdown dropleft">
                            <button class="btn btn-secondary btn-sm" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="icon-menu"></i>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                              <?php echo anchor("auth/group-permissions/".$group->id, '<i class="fa fa-key"></i> Hak Akses', array('class' => 'dropdown-item')) ;?>
                              <?php echo anchor("auth/edit_group/".$group->id, '<i class="fa fa-edit"></i> Edit', array('class' => 'dropdown-item')) ;?>
                              <?php if ($group->name !== 'admin'): ?>
                                <?php echo anchor("auth/delete_group/".$group->id, '<i class="fa fa-trash"></i> Hapus', array('class' => 'dropdown-item btn-delete')) ;?>
                              <?php endif; ?>
                            </div>
                          </div>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
  				</div>
        </div>
			</div>
		</div>
	</div>
</div>