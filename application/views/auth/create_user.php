<div class="row">
  <div class="col-md-12">
    <div class="card">
      <?php echo form_open(uri_string(), array('class' => 'form-horizontal'));?>
      <div class="card-header">
        <i class="nav-icon icon-user"></i> Tambah Pengguna
      </div>
      <div class="card-body">
              <div class="form-group row">
                <label class="col-md-2 col-form-label"><?php echo lang('create_user_fname_label', 'first_name');?></label>
                <div class="col-md-4">
                  <?php echo form_input($first_name);?>
                  <?php echo form_error('first_name', '<div class="invalid-feedback">', '</div>'); ?>
                </div>
                <label class="col-md-2 col-form-label"><?php echo lang('create_user_lname_label', 'last_name');?></label>
                <div class="col-md-4">
                  <?php echo form_input($last_name);?>
                  <?php echo form_error('last_name', '<div class="invalid-feedback">', '</div>'); ?>
                </div>
              </div>

              <?php if ($identity_column!=='email') : ?>
              <div class="form-group row">
                <label class="col-md-2 col-form-label"><?php echo lang('create_user_identity_label', 'identity'); ?></label>
                <div class="col-md-10">
                  <?php echo form_input($identity);?>
                  <?php echo form_error('identity', '<div class="invalid-feedback">', '</div>'); ?>
                </div>
              </div>
              <?php endif; ?>

              <div class="form-group row">
                <label class="col-md-2 col-form-label"><?php echo lang('create_user_company_label', 'company');?></label>
                <div class="col-md-10">
                  <?php echo form_input($company);?>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-2 col-form-label"><?php echo lang('create_user_email_label', 'email');?></label>
                <div class="col-md-10">
                  <?php echo form_input($email);?>
                  <?php echo form_error('email', '<div class="invalid-feedback">', '</div>'); ?>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-2 col-form-label"><?php echo lang('create_user_phone_label', 'phone');?> </label>
                <div class="col-md-10">
                  <?php echo form_input($phone);?>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-2 col-form-label"><?php echo lang('create_user_password_label', 'password');?></label>
                <div class="col-md-10">
                  <?php echo form_input($password);?>
                  <?php echo form_error('password', '<div class="invalid-feedback">', '</div>'); ?>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-2 col-form-label"><?php echo lang('create_user_password_confirm_label', 'password_confirm');?></label>
                <div class="col-md-10">
                  <?php echo form_input($password_confirm);?>
                  <?php echo form_error('password_confirm', '<div class="invalid-feedback">', '</div>'); ?>
                </div>
              </div>
      </div>

      <div class="card-footer">
          <?php echo form_button(array('content' => '<i class="fa fa-save"></i> Simpan', 'class' => 'btn btn-primary', 'type' => 'submit'));?>
          <?php echo anchor(base_url('auth'), '<i class="fa fa-arrow-left"></i> Kembali', array('class' => 'btn btn-warning'));?>
        
      </div>

        <?php echo form_close();?>
    </div>
  </div>
</div>
