<div class="row">
  <div class="col-md-12">
    
    <?php if (!empty($this->session->userdata('success'))) : ?>
    <div class="alert alert-success">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
      <?php echo $this->session->userdata('success'); ?>
    </div>
    <?php endif; ?>

    <div class="card">
      <div class="card-header">
        <i class="icon-lock"></i> Hak Akses User : <strong><?php echo $user->row()->username; ?></strong>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-12">
            <?php echo form_open(); ?>

            <?php foreach ($permissions as $key_modul => $modul) :?>

            <div class="card card-accent-primary">
              <div class="card-header">
                <a class="card-header-action collapsed" href="#" data-toggle="collapse" data-target="#collapse_<?php echo str_replace(" ", "_", $key_modul); ?>" aria-expanded="false" aria-controls="collapse_<?php echo str_replace(" ", "_", $key_modul); ?>">
                  <i class="icon-list"></i> <?php echo ucwords($key_modul); ?>
                </a>
                <div class="card-header-actions">
                  <a class="card-header-action collapsed" href="#" data-toggle="collapse" data-target="#collapse_<?php echo str_replace(" ", "_", $key_modul); ?>" aria-expanded="false" aria-controls="collapse_<?php echo str_replace(" ", "_", $key_modul); ?>">
                    <i class="fa fa-chevron-down"></i>
                  </a>
                </div>
              </div>
              <div class="collapse" id="collapse_<?php echo str_replace(" ", "_", $key_modul); ?>">
                <div class="card-body">
                  
                  <?php foreach ($modul as $key_controller => $controller) :?>
                  <div class="card card-accent-success">
                    <div class="card-header">
                      <a class="card-header-action collapsed" href="#" data-toggle="collapse" data-target="#collapse_controller_<?php echo str_replace(" ", "_", $key_controller); ?>" aria-expanded="false" aria-controls="collapse_controller_<?php echo str_replace(" ", "_", $key_controller); ?>">
                        <i class="icon-doc"></i> <?php echo ucwords((implode(" ", explode("_", $key_controller)))); ?>
                      </a>
                      <div class="card-header-actions">
                        <a class="card-header-action collapsed" href="#" data-toggle="collapse" data-target="#collapse_controller_<?php echo str_replace(" ", "_", $key_controller); ?>" aria-expanded="false" aria-controls="collapse_controller_<?php echo str_replace(" ", "_", $key_controller); ?>">
                          <i class="fa fa-chevron-down"></i>
                        </a>
                      </div>
                    </div>
                    <div class="collapse" id="collapse_controller_<?php echo str_replace(" ", "_", $key_controller); ?>">
                      <div class="card-body">
                        <div class="table-responsive">
                          <table class="table table-hover table-bordered" id="data-bidang">
                            <thead>
                              <tr>
                                  <th>Permission Name</th>
                                  <th>Permission Key</th>
                                  <th style="width: 10%;">Allow</th>
                                  <th style="width: 10%;">Deny</th>
                                  <th style="width: 20%;">Inherited From Group</th>
                              </tr>
                            </thead>
                            <tbody>
                            <?php
                                
                                foreach ($controller as $key_action => $v) :
                            ?>
                              <tr>
                                  <td style="vertical-align: middle;"><?php echo $v['name']; ?></td>
                                  <td style="vertical-align: middle;"><?php echo $v['key']; ?></td>
                                  <td style="vertical-align: middle;">
                                      <div class="animated-radio-button">
                                          <label>
                                              <?php echo form_radio("perm_{$v['id']}", '1', set_radio("perm_{$v['id']}", '1', $this->ion_auth_acl->is_allowed($v['key'], $users_permissions))); ?>
                                              <span class="label-text">Allow</span>
                                          </label>
                                      </div>
                                  </td>
                                  <td style="vertical-align: middle;">
                                      <div class="animated-radio-button">
                                          <label>
                                              <?php echo form_radio("perm_{$v['id']}", '0', set_radio("perm_{$v['id']}", '0', $this->ion_auth_acl->is_denied($v['key'], $users_permissions))) ?>
                                              <span class="label-text">Deny</span>
                                          </label>
                                      </div>
                                  </td>
                                  <td>
                                      <div class="animated-radio-button">
                                          <label>
                                              <?php echo form_radio("perm_{$v['id']}", 'X', set_radio("perm_{$v['id']}", 'X', ( $this->ion_auth_acl->is_inherited($v['key'], $users_permissions) || ! array_key_exists($v['key'], $users_permissions)) ? TRUE : FALSE)); ?> <span class="label-text">(Inherit <?php echo ($this->ion_auth_acl->is_inherited($v['key'], $group_permissions, 'value')) ? "Allow" : "Deny"; ?>)</span>
                                          </label>
                                      </div>
                                  </td>
                              </tr>
                              <?php endforeach; ?>
                            </tbody>
                          </table>
                        </div>

                      </div>
                    </div>
                  </div>
                  
                    <?php endforeach; ?>

                </div>
              </div>
            </div>
            <?php endforeach; ?>
                <?php echo form_button(array('type'=>'submit'), '<i class="fa fa-save"></i> Simpan','class="btn btn-primary"'); ?>
                <a href="<?php echo base_url('auth') ?>" class="btn btn-secondary"><i class="fa fa-arrow-left"></i>Kembali</a>
          <?php echo form_close(); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>