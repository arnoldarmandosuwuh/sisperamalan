<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Aktual_model extends CI_Model
{
  private $table = 'data_aktual';
  private $table1 = 'kota';
  private $table2 = 'tahun';

  public function get_data()
  {
    $this->db->select('data_aktual.id, kota.kota, tahun.tahun, data_aktual.data_aktual');
    $this->db->where('data_aktual.aktif', 1);
    $this->db->where('kota.aktif', 1);
    $this->db->where('tahun.aktif', 1);
    $this->db->join($this->table1, 'kota.id = data_aktual.id_kota');
    $this->db->join($this->table2, 'tahun.id = data_aktual.id_tahun');
    return $this->db->get($this->table);
  }

  private function check_data($kota, $tahun)
  {
    $this->db->where('aktif', 1);
    $this->db->where('id_kota', $kota);
    $this->db->where('id_tahun', $tahun);
    $check = $this->db->get($this->table)->num_rows();

    if ($check > 0) {
      return true;
    } else {
      return false;
    }
  }

  public function insert($data)
  {
    return $this->db->insert($this->table, $data);
  }

  public function insert_tahun($tahun)
  {
    if ($this->db->insert($this->table2, array('tahun' => $tahun))) {
      return $this->db->get_where($this->table2, array('tahun' => $tahun, 'aktif' => 1))->row_array();
    }
  }

  public function check_data_aktual($kota, $tahun, $aktual)
  {
    $this->db->where('tahun', $tahun);
    $this->db->where('id_kota', $kota);
    $this->db->where('data_aktual', $aktual);
    $this->db->where('data_aktual.aktif', 1);
    $this->db->join($this->table2, 'tahun.id = data_aktual.id_tahun');
    return $this->db->get($this->table);
  }

  public function check_tahun($tahun)
  {
    return $this->db->get_where($this->table2, array('tahun' => $tahun, 'aktif' => 1));
  }

  public function get_data_by_id($id)
  {
    return $this->db->get_where($this->table, array('id' => $id))->row_array();
  }

  public function update($id, $data)
  {
    $this->db->where('id', $id);
    $this->db->update($this->table, $data);

    return $this->db->affected_rows();
  }

  public function delete($id)
  {
    $this->db->set('aktif', 0);
    $this->db->where('id', $id);
    $this->db->update($this->table);

    return $this->db->affected_rows();
  }

  public function get_all_kota()
  {
    return $this->db->get_where($this->table1, array('aktif' => 1));
  }

  public function get_all_tahun()
  {
    return $this->db->get_where($this->table2, array('aktif' => 1));
  }
}
