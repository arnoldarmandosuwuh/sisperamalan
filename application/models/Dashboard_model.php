<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard_model extends CI_Model
{
    private $table = 'tahun';
    private $table2 = 'kota';
    private $table3 = 'data_aktual';

    public function get_data($tahun)
    {
        $this->db->select('kota, tahun, data_aktual');
        $this->db->where('kota.aktif', 1);
        $this->db->where('tahun.aktif', 1);
        $this->db->where('data_aktual.aktif', 1);
        $this->db->where('tahun', $tahun);
        $this->db->join($this->table, 'tahun.id = data_aktual.id_tahun');
        $this->db->join($this->table2, 'kota.id = data_aktual.id_kota');
        return $this->db->get($this->table3);
    }

    public function get_last_year()
    {
        $this->db->select_max('tahun');
        return $this->db->get_where($this->table, array('aktif' => 1));
    }

    public function insert($data)
    {
        return $this->db->insert($this->table, $data);
    }

    public function get_data_by_id($id)
    {
        return $this->db->get_where($this->table, array('id' => $id))->row_array();
    }

    public function update($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update($this->table, $data);

        return $this->db->affected_rows();
    }

    public function delete($id)
    {
        $this->db->set('aktif', 0);
        $this->db->where('id', $id);
        $this->db->update($this->table);

        return $this->db->affected_rows();
    }
}
