<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Peramalan_model extends CI_Model
{
    private $table = 'hasil_peramalan';
    private $table1 = 'data_aktual';
    private $table2 = 'tahun';
    private $table3 = 'kota';
    private $table4 = 'alpha';

    public function get_all_kota()
    {
        $this->db->select('kota.*');
        $this->db->join($this->table1, 'kota.id = data_aktual.id_kota');
        $this->db->where('kota.aktif', 1);
        $this->db->order_by('id', "asc");
        $this->db->group_by('id_kota');
        return $this->db->get($this->table3);
    }

    public function get_all_tahun()
    {
        $this->db->where('aktif', 1);
        $this->db->order_by('tahun', "asc");
        return $this->db->get($this->table2);
    }

    public function get_all_alpha()
    {
        $this->db->order_by('alpha', "asc");
        return $this->db->get($this->table4);
    }

    public function get_tahun_by_id($id)
    {
      return $this->db->get_where($this->table2, array('id' => $id))->row_array();
    }

    public function get_data_aktual($data)
    {
        $this->db->select('data_aktual.id, kota, tahun, data_aktual');
        $this->db->where('kota.id', $data['id_kota']);
        $this->db->where('tahun.tahun >= ', $data['tahun1']);
        $this->db->where('tahun.tahun <= ', $data['tahun2']);
        $this->db->where('data_aktual.aktif', 1);
        $this->db->join($this->table2, 'tahun.id = data_aktual.id_tahun');
        $this->db->join($this->table3, 'kota.id = data_aktual.id_kota');
        $this->db->order_by('tahun', "asc");
        return $this->db->get($this->table1);
    }

    public function get_peramalan($data)
    {
        $this->db->select('hasil_peramalan.id, id_kota, kota, id_tahun, tahun, data_aktual, alpha.alpha, pem1, pem2, expo1, expo2, hasil, fe');
        $this->db->join($this->table1, 'data_aktual.id = hasil_peramalan.id_data_aktual');
        $this->db->join($this->table4, 'alpha.id = hasil_peramalan.id_alpha');
        $this->db->join($this->table2, 'tahun.id = data_aktual.id_tahun');
        $this->db->join($this->table3, 'kota.id = data_aktual.id_kota');
        $this->db->where('alpha.id', $data['alpha']);
        $this->db->where('id_kota', $data['id_kota']);
        $this->db->where('tahun.tahun >= ', $data['tahun1']);
        $this->db->where('tahun.tahun <= ', $data['tahun2']);
        $this->db->where('data_aktual.aktif', 1);
        $this->db->order_by('tahun', "asc");
        return $this->db->get($this->table);
        
    }

    public function get_alpha_by_id($id)
    {
        return $this->db->get_where($this->table4, array('id' => $id))->row_array();
    }

    public function insert_hasil($data)
    {
        foreach ($data as $value)
        {
            $insert = $this->db->insert($this->table, $value);
        }
        return $insert;
    }

    public function delete_hasil($data)
    {
        foreach ($data as $value)
        {
            $delete = $this->db->delete($this->table, array('id' => $value['id']));
        }
        return $delete;
    }
}