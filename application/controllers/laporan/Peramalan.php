<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Peramalan extends App_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('laporan/peramalan_model');
    }

    public function index()
    {
        header("Access-Control-Allow-Origin: *");
        $data = array(
            'title'         => 'Data Peramalan',
            'breadcrumbs'   => array(
                array('title' => 'Beranda', 'url' => base_url()),
                array('title' => 'Peramalan', 'url' => base_url("laporan/peramalan")),
                array('title' => 'Data Peramalan'),
            ),
            'list_kota'     => $this->peramalan_model->get_all_kota()->result(),
            'list_tahun'    => $this->peramalan_model->get_all_tahun()->result(),
            'list_alpha'    => $this->peramalan_model->get_all_alpha()->result(),
            'list_data'     => array(),
            'mape'          => 0,
        );

        if (!$this->ion_auth_acl->has_permission('laporan-peramalan-read')) {
            $this->session->set_flashdata('error', 'Anda tidak dapat mengakses halaman ' . $data['title'] . '.');
            redirect('dashboard', 'refresh');
        }

        $post = $this->input->post();

        if (!empty($post)) {
            $this->form_validation->set_rules('id_kota', 'Kota', 'required');
            $this->form_validation->set_rules('periode1', 'Periode Awal', 'required');
            $this->form_validation->set_rules('periode2', 'Periode Akhir', 'required|callback_periode_check');
            $this->form_validation->set_rules('alpha', '&alpha;', 'required');
        }

        if ($this->form_validation->run()) {
            $post['tahun1'] = $this->peramalan_model->get_tahun_by_id($post['periode1'])['tahun'];
            $post['tahun2'] = $this->peramalan_model->get_tahun_by_id($post['periode2'])['tahun'];
            $periods = $post['tahun2'] - $post['tahun1'];
            if ($periods > 10) {
                $post['periode'] = 5;
            } else if ($periods >= 5) {
                $post['periode'] = 3;
            } else {
                $post['periode'] = 2;
            }


            $data_aktual = $this->peramalan_model->get_data_aktual($post)->result_array();

            if (empty($data_aktual)) {
                $this->session->set_flashdata('error', 'Data tidak tersedia.');
            } else {
                $list_data = $this->hitung_peramalan($post);
                $data['list_data'] = $list_data['list_data'];
                $data['mape'] = $list_data['mape'];
                $data['aktual'] = $list_data['aktual'];
                $data['periode'] = $list_data['periode'];
                $data['ramal'] = $list_data['ramal'];
            }
        }

        $this->layout->view('laporan/peramalan/data_peramalan', $data);
    }

    public function periode_check($str)
    {
        if (!empty($this->input->post('periode1'))) {
            $periode1 = $this->input->post('periode1');
            $tahun1 = $this->peramalan_model->get_tahun_by_id($periode1)['tahun'];
            $tahun2 = $this->peramalan_model->get_tahun_by_id($str)['tahun'];

            if ($tahun2 > $tahun1) {
                return TRUE;
            } else {
                $this->form_validation->set_message('periode_check', 'Periode akhir harus lebih besar dari periode awal');
                return FALSE;
            }
        }
    }

    public function hitung_peramalan($post)
    {
        $cek_hasil = $this->peramalan_model->get_peramalan($post)->result_array();
        if (empty($cek_hasil)) {
            $this->insert_hasil($post);
        } else {
            $this->peramalan_model->delete_hasil($cek_hasil);
            $this->insert_hasil($post);
        }
        $hasil = $this->peramalan_model->get_peramalan($post)->result_array();
        $data['list_data'] = array();

        $x = 1;
        $fe = 0;
        $length = count($hasil);
        $tahun_ramal = $hasil[$length-1]['tahun'];

        for ($i = 0; $i < ($length + $post['periode']); $i++) {
            if ($i >= 0 && $i < $length) {
                $fe = $fe + $hasil[$i]['fe'];
                $data['list_data'][$i]['kota'] = $hasil[$i]['kota'];
                $data['list_data'][$i]['alpha'] = $hasil[$i]['alpha'];
                $data['list_data'][$i]['tahun'] = $hasil[$i]['tahun'];
                $data['list_data'][$i]['data_aktual'] = number_format((float)$hasil[$i]['data_aktual'], 2, ',', '.');
                $data['list_data'][$i]['hasil'] = number_format((float)$hasil[$i]['hasil'], 2, ',', '.');
                $data['list_data'][$i]['fe'] = number_format((float)$hasil[$i]['fe'], 2, ',', '.');
            } else {
                $data['list_data'][$i]['kota'] = $hasil[$length - 1]['kota'];
                $data['list_data'][$i]['alpha'] = $hasil[$length - 1]['alpha'];
                $data['list_data'][$i]['tahun'] = $tahun_ramal = $tahun_ramal+1;
                $data['list_data'][$i]['data_aktual'] = "-";
                $data['list_data'][$i]['hasil'] = number_format((float)$hasil[$length - 1]['expo1'] + ($hasil[$length - 1]['expo2'] * $x), 2, ',', '.');
                $data['list_data'][$i]['fe'] = "-";
                $x++;
            }
        }
        if ($fe != 0) {
            $data['mape'] = $fe / $length;
        } else {
            $data['mape'] = 0;
        }

        for ($i = 0; $i < $length; $i++) {
            $data['aktual'][] = (float)$data['list_data'][$i]['data_aktual'];
        }
        foreach ($data['list_data'] as $val) {
            $data['periode'][] = $val['tahun'];
            $data['ramal'][] = (float)$val['hasil'];
        }

        return $data;
    }

    public function insert_hasil($post)
    {
        $data_aktual = $this->peramalan_model->get_data_aktual($post)->result_array();
        $alpha = $this->peramalan_model->get_alpha_by_id($post['alpha']);

        $length = count($data_aktual);
        $data_ramal = array();

        for ($key = 0; $key < $length; $key++) {
            if ($key === 0) {
                $data_ramal[$key]['id_data_aktual'] = $data_aktual[$key]['id'];
                $data_ramal[$key]['id_alpha'] = $post['alpha'];
                $data_ramal[$key]['pem1'] = $data_aktual[$key]['data_aktual'];
                $data_ramal[$key]['pem2'] = $data_aktual[$key]['data_aktual'];
                $data_ramal[$key]['expo1'] = $data_aktual[$key]['data_aktual'];
                $data_ramal[$key]['expo2'] = ($alpha['alpha'] / (1 - $alpha['alpha']) * ($data_ramal[$key]['pem1'] - $data_ramal[$key]['pem2']));
                $data_ramal[$key]['hasil'] = $data_ramal[$key]['expo1'] + $data_ramal[$key]['expo2'];
                $data_ramal[$key]['fe'] = abs((($data_aktual[$key]['data_aktual'] - $data_ramal[$key]['hasil']) / $data_aktual[$key]['data_aktual']) * 100);
            } else {
                $data_ramal[$key]['id_data_aktual'] = $data_aktual[$key]['id'];
                $data_ramal[$key]['id_alpha'] = $post['alpha'];
                $data_ramal[$key]['pem1'] = ($alpha['alpha'] * $data_aktual[$key]['data_aktual']) + (1 - $alpha['alpha']) * $data_ramal[$key - 1]['pem1'];
                $data_ramal[$key]['pem2'] = ($alpha['alpha'] * $data_ramal[$key]['pem1']) + (1 - $alpha['alpha']) * $data_ramal[$key - 1]['pem2'];
                $data_ramal[$key]['expo1'] = (2 * $data_ramal[$key]['pem1']) - $data_ramal[$key]['pem2'];
                $data_ramal[$key]['expo2'] = ($alpha['alpha'] / (1 - $alpha['alpha']) * ($data_ramal[$key]['pem1'] - $data_ramal[$key]['pem2']));
                $data_ramal[$key]['hasil'] = $data_ramal[$key]['expo1'] + $data_ramal[$key]['expo2'];
                $data_ramal[$key]['fe'] = abs((($data_aktual[$key]['data_aktual'] - $data_ramal[$key]['hasil']) / $data_aktual[$key]['data_aktual']) * 100);
            }
        }

        $this->peramalan_model->insert_hasil($data_ramal);
        $this->session->set_flashdata('success', 'Perhitungan peramalan berhasil.');
    }
}
