<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Aktual extends App_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("transaksi/aktual_model");
    }

    public function index()
    {
        header("Access-Control-Allow-Origin: *");
        $data = array('title' => 'Data Pendaftar');

        if (!$this->ion_auth_acl->has_permission('transaksi-aktual-read')) {
            $this->session->set_flashdata('error', 'Anda tidak dapat mengakses halaman ' . $data['title'] . '.');
            redirect('dashboard', 'refresh');
        }

        $data['breadcrumbs'] = array(
            array('title' => 'Beranda', 'url' => base_url()),
            array('title' => 'Pendaftar', 'url' => base_url("transaksi/aktual")),
            array('title' => 'Data Pendaftar'),
        );

        $data['list_data'] = $this->aktual_model->get_data()->result();
        $this->layout->view('transaksi/aktual/data_aktual', $data);
    }

    public function import()
    {
        $form = $this->_populate_form_import($this->input->post());
        $data = array(
            'form'  => $form,
            'title' => 'Tambah Data Pendaftar',
            'breadcrumbs'   => array(
                array('title' => 'Beranda', 'url' => base_url()),
                array('title' => 'Pendaftar', 'url' => base_url("transaksi/aktual")),
                array('title' => 'Tambah Data Pendaftar'),
            ),
        );

        if (!$this->ion_auth_acl->has_permission('transaksi-aktual-create')) {
            $this->session->set_flashdata('error', 'Anda tidak dapat mengakses halaman ' . $data['title'] . '.');
            redirect('dashboard', 'refresh');
        }
        if (!empty($_FILES['csv']['name'])) {
            $filename = $_FILES['csv']['tmp_name'];
            $result =   $this->csvreader->parse_file($filename);

            foreach ($result as $key => $value) {
                $val = array_values($value);
                $post['id_kota'] = $this->input->post()['id_kota'];
                $post['data_aktual'] = $val[1];

                $check_aktual = $this->aktual_model->check_data_aktual($post['id_kota'], $val[0], $val[1]);

                if ($check_aktual->num_rows() <= 0) {
                    $tahun = $this->aktual_model->check_tahun($val[0])->row_array();

                    if (empty($tahun)) {
                        $tahun = $this->aktual_model->insert_tahun($val[0]);
                    }
                    $post['id_tahun'] = $tahun['id'];

                    if ($this->aktual_model->insert($post)) {
                        $this->session->set_flashdata('success', 'Data telah berhasil ditambahkan.');
                    }
                } else {
                    continue;
                }
            }
            redirect('transaksi/aktual', 'refresh');
        } else {
            $this->layout->view('transaksi/aktual/form_aktual_import', $data);
        }
    }

    public function add()
    {
        $form = $this->_populate_form($this->input->post());
        $data = array(
            'form'  => $form,
            'title' => 'Tambah Data Pendaftar',
            'breadcrumbs'   => array(
                array('title' => 'Beranda', 'url' => base_url()),
                array('title' => 'Pendaftar', 'url' => base_url("transaksi/aktual")),
                array('title' => 'Tambah Data Pendaftar'),
            ),
        );

        if (!$this->ion_auth_acl->has_permission('transaksi-aktual-create')) {
            $this->session->set_flashdata('error', 'Anda tidak dapat mengakses halaman ' . $data['title'] . '.');
            redirect('dashboard', 'refresh');
        }

        if ($this->form_validation->run()) {
            $post = $this->input->post();
            $this->aktual_model->insert($post);
            $this->session->set_flashdata('success', 'Data telah berhasil ditambahkan.');
            redirect('transaksi/aktual', 'refresh');
        } else {
            $this->layout->view('transaksi/aktual/form_aktual', $data);
        }
    }

    public function edit($id)
    {
        if (!empty($this->input->post())) {
            $data = $this->input->post();
        } else {
            $data = $this->aktual_model->get_data_by_id($id);
        }

        $form = $this->_populate_form($data);

        $data = array(
            'form'          => $form,
            'title'         => 'Edit Data Pendaftar',
            'breadcrumbs'   => array(
                array('title' => 'Beranda', 'url' => base_url()),
                array('title' => 'Pendaftar', 'url' => base_url("transaksi/aktual")),
                array('title' => 'Edit Data Pendaftar'),
            ),
            'url_form'      => base_url('transaksi/aktual/edit/' . $id)
        );

        if (!$this->ion_auth_acl->has_permission('transaksi-aktual-update')) {
            $this->session->set_flashdata('error', 'Anda tidak dapat mengakses halaman ' . $data['title'] . '.');
            redirect('dashboard', 'refresh');
        }

        if ($this->form_validation->run()) {
            $post = $this->input->post();
            $this->aktual_model->update($id, $post);
            $this->session->set_flashdata('success', 'Data telah berhasil dirubah.');
            redirect('transaksi/aktual', 'refresh');
        } else {
            $this->layout->view('transaksi/aktual/form_aktual', $data);
        }
    }

    public function delete($id)
    {
        $data = array('title' => 'Hapus Aktual');

        if (!$this->ion_auth_acl->has_permission('transaksi-aktual-delete')) {
            $this->session->set_flashdata('error', 'Anda tidak dapat mengakses halaman ' . $data['title'] . '.');
            redirect('dashboard', 'refresh');
        }

        $this->aktual_model->delete($id);
        $this->session->set_flashdata('success', 'Data telah berhasil dihapus.');
        redirect('transaksi/aktual', 'refresh');
    }

    private function get_all_kota()
    {
        $data = $this->aktual_model->get_all_kota()->result();
        $result = array('' => '- Pilih Kota -');
        foreach ($data as $key) {
            $result[$key->id] = $key->kota;
        }
        return $result;
    }

    private function get_all_tahun()
    {
        $data = $this->aktual_model->get_all_tahun()->result();
        $result = array('' => '- Pilih Tahun -');
        foreach ($data as $key) {
            $result[$key->id] = $key->tahun;
        }
        return $result;
    }

    public function _populate_form($data)
    {
        $req_sign = ' <span class="text-danger"> * </span>';
        $form = array(
            'id_kota' => array(
                'label'  => 'Nama Kota' . $req_sign,
                'input' => array(
                    'name'          => 'id_kota',
                    'id'            => 'id_kota',
                    'class'         => 'form-control app-select2',
                    'options'       => $this->get_all_kota()
                )
            ),
            'id_tahun' => array(
                'label'  => 'Tahun' . $req_sign,
                'input' => array(
                    'name'          => 'id_tahun',
                    'id'            => 'id_tahun',
                    'class'         => 'form-control app-select2',
                    'options'       => $this->get_all_tahun()
                )
            ),
            'data_aktual' => array(
                'label' => 'Data Pendaftar',
                'input' => array(
                    'name'          => 'data_aktual',
                    'id'            => 'data_aktual',
                    'class'         => 'form-control',
                    'type'          => 'number',
                    'step'          => 'any',
                    'placeholder'   => 'Masukkan data pendaftar'
                )
            ),
        );

        $this->form_validation->set_rules('id_kota', 'Nama Kota', 'required');
        $this->form_validation->set_rules('id_tahun', 'Nama Kota', 'required');
        $this->form_validation->set_rules('data_aktual', 'Data Pendaftar', 'required|greater_than[0]');

        foreach ($data as $key => $value) {
            if (in_array($key, array('id_kota')) || in_array($key, array('id_tahun'))) {
                $form[$key]['input']['selected'] = $value;
                continue;
            }
            $form[$key]['input']['value'] = $value;
        }

        return $form;
    }

    public function _populate_form_import($data)
    {
        $req_sign = ' <span class="text-danger"> * </span>';
        $form = array(
            'id_kota' => array(
                'label'  => 'Nama Kota' . $req_sign,
                'input' => array(
                    'name'          => 'id_kota',
                    'id'            => 'id_kota',
                    'class'         => 'form-control app-select2',
                    'options'       => $this->get_all_kota()
                )
            ),
            'csv' => array(
                'label'  => 'File CSV' . $req_sign,
                'input' => array(
                    'name'          => 'csv',
                    'id'            => 'csv',
                    'class'         => 'form-control-file',
                    'placeholder'   => 'Masukkan File CSV',
                    'accept'        => '.csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel'
                )
            ),
        );

        if (empty($_FILES['csv']['name'])) {
            $this->form_validation->set_rules('csv', 'File CSV', 'required');
        }
        $this->form_validation->set_rules('id_kota', 'Nama Kota', 'required');

        foreach ($data as $key => $value) {
            if (in_array($key, array('id_kota'))) {
                $form[$key]['input']['selected'] = $value;
                continue;
            }
            $form[$key]['input']['value'] = $value;
        }

        return $form;
    }
}
