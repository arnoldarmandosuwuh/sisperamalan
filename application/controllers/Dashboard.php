<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends App_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("dashboard_model");
    }

    public function index()
    {
        header("Access-Control-Allow-Origin: *");
        $last_year = $this->dashboard_model->get_last_year()->row_array();
        $list_data = $this->dashboard_model->get_data($last_year['tahun'])->result_array();

        foreach ($list_data as $value) {
            $kota[] = $value['kota'];
            $data_aktual[] = $value['data_aktual'];
        }
        $data = array(
            'title' => 'Selamat Datang',
            'breadcrumbs' => array(
                array('title' => 'Beranda', 'url' => base_url()),
                array('title' => 'Selamat Datang di Halaman Beranda', 'url' => base_url()),
            ),
            'title_card' => 'Data Pendaftar ' . $last_year['tahun'],
            'kota' => $kota,
            'data_aktual' => $data_aktual,
            'name_user' => $this->ion_auth->user()->row()->first_name . ' ' . $this->ion_auth->user()->row()->last_name,
        );

        // echo '<pre>' . var_dump($kota) . '</pre>';
        // exit();

        $this->layout->view('dashboard/home', $data);
    }
}
