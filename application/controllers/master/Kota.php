<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kota extends App_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model("master/kota_model");   
    }

    public function index()
    {
        header("Access-Control-Allow-Origin: *");
        $data = array('title' => 'Data Kota');

        if(!$this->ion_auth_acl->has_permission('master-kota-read')){
            $this->session->set_flashdata('error', 'Anda tidak dapat mengakses halaman '.$data['title'].'.');
            redirect('dashboard', 'refresh');
        }

        $data['breadcrumbs'] = array(
            array('title' => 'Beranda', 'url' => base_url()),
            array('title' => 'Kota', 'url' => base_url("master/kota")),
			array('title' => 'Data Kota'),
        );
        
        $data['list_data'] = $this->kota_model->get_data()->result();
        $this->layout->view('master/kota/data_kota', $data);
    }

    public function add()
    {
        $form = $this->_populate_form($this->input->post());
        $data = array(
            'form'  => $form,
            'title' => 'Tambah Kota',
            'breadcrumbs'   => array(
                array('title' => 'Beranda', 'url' => base_url()),
                array('title' => 'Kota', 'url' => base_url("master/kota")),
                array('title' => 'Tambah Kota'),
            )
        );
        
        if(!$this->ion_auth_acl->has_permission('master-kota-create')){
            $this->session->set_flashdata('error', 'Anda tidak dapat mengakses halaman '.$data['title'].'.');
            redirect('dashboard', 'refresh');
        }

        if ($this->form_validation->run()) {
            $post = $this->input->post();
            $this->kota_model->insert($post);
            $this->session->set_flashdata('success', 'Data telah berhasil ditambahkan.');
            redirect('master/kota', 'refresh');
        } else {
            $this->layout->view('master/kota/form_kota', $data);
        }
    }

    public function edit($id)
    {
        if (!empty($this->input->post())){
            $data = $this->input->post(); 
        } else {
            $data = $this->kota_model->get_data_by_id($id);
        }

        $form = $this->_populate_form($data);

        $data = array(
            'form'          => $form,
            'title'         => 'Edit Kota',
            'breadcrumbs'   => array(
                array('title' => 'Beranda', 'url' => base_url()),
                array('title' => 'Kota', 'url' => base_url("master/kota")),
                array('title' => 'Edit Kota'),
            ),
            'url_form'      => base_url('master/kota/edit/'.$id)
        );

        if(!$this->ion_auth_acl->has_permission('master-kota-update')){
            $this->session->set_flashdata('error', 'Anda tidak dapat mengakses halaman '.$data['title'].'.');
            redirect('dashboard', 'refresh');
        }

        if ($this->form_validation->run()) {
            $post = $this->input->post();
            $this->kota_model->update($id, $post);
            $this->session->set_flashdata('success', 'Data telah berhasil dirubah.');
            redirect('master/kota', 'refresh');
        } else {
            $this->layout->view('master/kota/form_kota', $data);
        }
    }

    public function delete($id)
    {
        $data = array('title' => 'Hapus Kota');

        if(!$this->ion_auth_acl->has_permission('master-kota-delete')){
            $this->session->set_flashdata('error', 'Anda tidak dapat mengakses halaman '.$data['title'].'.');
            redirect('dashboard', 'refresh');
        }

        $this->kota_model->delete($id);
        $this->session->set_flashdata('success', 'Data telah berhasil dihapus.');
        redirect('master/kota', 'refresh');
    }

    public function _populate_form($data)
    {
        $req_sign = ' <span class="text-danger"> * </span>';
        $form = array(
            'kota' => array(
                'label' => 'Kota'.$req_sign,
                'input' => array(
                    'name'          => 'kota',
                    'id'            => 'kota',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Kota'
                )
            ),

            'keterangan' => array(
                'label' => 'Keterangan',
                'input' => array(
                    'name'          => 'keterangan',
                    'id'            => 'keterangan',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Keterangan'
                )
            ),
        );

        $this->form_validation->set_rules('kota', 'Kota', 'required');

        foreach ($data as $key => $value) {
            $form[$key]['input']['value'] = $value;
        }
        
        return $form;
    }

}