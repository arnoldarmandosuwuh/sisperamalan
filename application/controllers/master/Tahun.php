<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tahun extends App_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model("master/tahun_model");   
    }

    public function index()
    {
        header("Access-Control-Allow-Origin: *");
        $data = array('title' => 'Data Tahun');

        if(!$this->ion_auth_acl->has_permission('master-tahun-read')){
            $this->session->set_flashdata('error', 'Anda tidak dapat mengakses halaman '.$data['title'].'.');
            redirect('dashboard', 'refresh');
        }

        $data['breadcrumbs'] = array(
            array('title' => 'Beranda', 'url' => base_url()),
            array('title' => 'Tahun', 'url' => base_url("master/tahun")),
			array('title' => 'Data Tahun'),
        );
        
        $data['list_data'] = $this->tahun_model->get_data()->result();
        $this->layout->view('master/tahun/data_tahun', $data);
    }

    public function add()
    {
        $form = $this->_populate_form($this->input->post());
        $data = array(
            'form'  => $form,
            'title' => 'Tambah Tahun',
            'breadcrumbs'   => array(
                array('title' => 'Beranda', 'url' => base_url()),
                array('title' => 'Tahun', 'url' => base_url("master/tahun")),
                array('title' => 'Tambah Tahun'),
            )
        );
        
        if(!$this->ion_auth_acl->has_permission('master-tahun-create')){
            $this->session->set_flashdata('error', 'Anda tidak dapat mengakses halaman '.$data['title'].'.');
            redirect('dashboard', 'refresh');
        }

        if ($this->form_validation->run()) {
            $post = $this->input->post();
            $this->tahun_model->insert($post);
            $this->session->set_flashdata('success', 'Data telah berhasil ditambahkan.');
            redirect('master/tahun', 'refresh');
        } else {
            $this->layout->view('master/tahun/form_tahun', $data);
        }
    }

    public function edit($id)
    {
        if (!empty($this->input->post())){
            $data = $this->input->post(); 
        } else {
            $data = $this->tahun_model->get_data_by_id($id);
        }

        $form = $this->_populate_form($data);

        $data = array(
            'form'          => $form,
            'title'         => 'Edit Tahun',
            'breadcrumbs'   => array(
                array('title' => 'Beranda', 'url' => base_url()),
                array('title' => 'Tahun', 'url' => base_url("master/tahun")),
                array('title' => 'Edit Tahun'),
            ),
            'url_form'      => base_url('master/tahun/edit/'.$id)
        );

        if(!$this->ion_auth_acl->has_permission('master-tahun-update')){
            $this->session->set_flashdata('error', 'Anda tidak dapat mengakses halaman '.$data['title'].'.');
            redirect('dashboard', 'refresh');
        }

        if ($this->form_validation->run()) {
            $post = $this->input->post();
            $this->tahun_model->update($id, $post);
            $this->session->set_flashdata('success', 'Data telah berhasil dirubah.');
            redirect('master/tahun', 'refresh');
        } else {
            $this->layout->view('master/tahun/form_tahun', $data);
        }
    }

    public function delete($id)
    {
        $data = array('title' => 'Hapus Tahun');

        if(!$this->ion_auth_acl->has_permission('master-tahun-delete')){
            $this->session->set_flashdata('error', 'Anda tidak dapat mengakses halaman '.$data['title'].'.');
            redirect('dashboard', 'refresh');
        }

        $this->tahun_model->delete($id);
        $this->session->set_flashdata('success', 'Data telah berhasil dihapus.');
        redirect('master/tahun', 'refresh');
    }

    public function _populate_form($data)
    {
        $req_sign = ' <span class="text-danger"> * </span>';
        $form = array(
            'tahun' => array(
                'label' => 'Tahun'.$req_sign,
                'input' => array(
                    'name'          => 'tahun',
                    'id'            => 'tahun',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Tahun'
                )
            ),

            'keterangan' => array(
                'label' => 'Keterangan',
                'input' => array(
                    'name'          => 'keterangan',
                    'id'            => 'keterangan',
                    'class'         => 'form-control',
                    'placeholder'   => 'Masukkan Keterangan'
                )
            ),
        );

        $this->form_validation->set_rules('tahun', 'Tahun', 'required');

        foreach ($data as $key => $value) {
            $form[$key]['input']['value'] = $value;
        }
        
        return $form;
    }

}