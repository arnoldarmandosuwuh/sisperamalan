<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------
| Navigation Menu untuk Halaman Dashboard
| -------------------------------------------------------------------
| Config untuk halaman admin dashboard dengan value array sebagai berikut:
| - icon : icon pada menu (https://coreui.io/demo/#icons/simple-line-icons.html)
| - url : url untuk menu navigasi
| - submenu : untuk menempatkan sub menu dalam format array, kosongi jika menu tidak memiliki submenu
| - permission : nama permission untuk hak akses menu
*/
$config['dashboard'] = array(
    'Beranda' => array(
        'icon'=> 'nav-icon icon-home',
        'url' => base_url('dashboard'),
        'permission' => array(
            'dashboard-beranda-read'
        )
    ),


    'Master' => array(
        'icon' => 'nav-icon icon-grid',
        'submenu'=> array(
            'Tahun'  => array(
                'url' => base_url('master/tahun'),
                'icon'=> 'nav-icon icon-calendar',
                'permission' => 'master-tahun-read'
            ),
            'Kota'  => array(
                'url' => base_url('master/kota'),
                'icon'=> 'nav-icon icon-map',
                'permission' => 'master-kota-read'
            ),
        ),
        'permission' => array(
            'master-tahun-read',
            'master-kota-read'
        )
    ),

    'Transaksi' => array(
        'icon' => 'nav-icon icon-grid',
        'submenu'=> array(
            'Data Pendaftar'  => array(
                'url' => base_url('transaksi/aktual'),
                'icon'=> 'nav-icon icon-note',
                'permission' => 'transaksi-aktual-read'
            ),
        ),
        'permission' => array(
            'transaksi-aktual-read'
        )
    ),

    'Laporan' => array(
        'icon' => 'nav-icon icon-grid',
        'submenu'=> array(
            'Peramalan'  => array(
                'url' => base_url('laporan/peramalan'),
                'icon'=> 'nav-icon icon-chart',
                'permission' => 'laporan-peramalan-read'
            ),
        ),
        'permission' => array(
            'laporan-peramalan-read',
        )
    ),

    'Pengaturan' => array(
        'icon' => 'nav-icon icon-settings',
        'submenu'=> array(
            'Pengguna'  => array(
                'url' => base_url('auth'),
                'icon'=> 'nav-icon icon-user',
                'permission' => 'pengaturan-pengguna-read'
            ),

            'Hak Akses' => array(
                'url' => base_url('permission'),
                'icon'=> 'nav-icon icon-lock',
                'permission' => 'pengaturan-hak_akses-read'
            ),
        ),
        'permission' => array(
            'pengaturan-hak_akses-read',
            'pengaturan-pengguna-read',
        )
    ),
);


/*
| -------------------------------------------------------------------
| Navigation Menu untuk Halaman Frontend
| -------------------------------------------------------------------
| Config untuk halaman admin dashboard dengan value array sebagai berikut:
| - icon : icon pada menu menggunakan font awesome (https://fontawesome.com/v4.7.0/icons)
| - url : url untuk menu navigasi
| - submenu : untuk menempatkan sub menu dalam format array, kosongi jika menu tidak memiliki submenu
*/
$config['frontend'] = array();

