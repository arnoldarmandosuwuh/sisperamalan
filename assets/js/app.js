$(document).ready(function () {
	//fungsi untuk mengaktifkan datatable
	$(".table-datatable").DataTable({
		language: {
			url: base_url + "assets/vendors/datatables/indonesia.json",
		},
		columnDefs: [{ orderable: false, targets: -1 }],
	});

	$(".peramalan-datatable").DataTable({
		pageLength: 30,
		language: {
			url: base_url + "assets/vendors/datatables/indonesia.json",
		},
		columnDefs: [{ orderable: false, targets: -1 }],
	});

	//fungsi untuk menampilkan konfirmasi hapus data
	$(".btn-delete").on("click", function (e) {
		e.preventDefault();
		var urlhapus = $(this).attr("href");

		Swal.fire({
			title: "Konfirmasi",
			text: "Apakah Anda yakin akan menghapus data?",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#f86c6b",
			cancelButtonColor: "#20a8d8",
			confirmButtonText: "Hapus",
			cancelButtonText: "Tidak",
		}).then((result) => {
			if (result.value) {
				window.location.href = urlhapus;
			}
		});
	});

	setTimeout(function () {
		$(".alert").hide();
	}, 4000);

	// init select2
	$(".app-select2").select2({
		theme: "bootstrap4",
	});
});
